package cqu.edu.MVCDemo.controller;

import cqu.edu.MVCDemo.exception.HeroNameException;
import cqu.edu.MVCDemo.model.Hero;
import cqu.edu.MVCDemo.service.impl.HeroServiceImpl;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@CrossOrigin
@RestController
public class HeroController {
    @Resource
    HeroServiceImpl heroService;

    //get all heroes from mysql database
    @GetMapping("/heroes")
    public List<Hero> selectHeroes(){
        return heroService.selectHeroes();
    }

    //get heroes whose name includes the received name
    @GetMapping("/heroes/fuzzy")
    public List<Hero> selectHeroesFuzzy(@RequestParam String name){
        return heroService.selectHeroesFuzzy(name);
    }

    //get hero by id
    @GetMapping("/hero")
    public Hero selectHero(@RequestParam Integer id){
        Hero hero = new Hero();
        hero.setId(id);
        return heroService.selectHero(hero);
    }
    
    //create a hero
    @PostMapping("/hero")
    public Hero addHero(@RequestBody Hero hero) throws HeroNameException {
        if (null!=heroService.selectHeroByName(hero)){
            System.out.println("exits");
            throw new HeroNameException("英雄已经存在");
        }
       heroService.addHero(hero);
        return heroService.selectHeroByName(hero);
    }

    //update a hero
    @PutMapping("/hero")
    public boolean updateHero(@RequestBody Hero hero){
        return heroService.updateHero(hero)>0;
    }

    //delete a hero
    @DeleteMapping("/hero")
    public boolean deleteHero(@RequestBody Hero hero){
        return heroService.deleteHero(hero)>0;
    }

}
