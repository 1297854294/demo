package cqu.edu.MVCDemo.service;

import cqu.edu.MVCDemo.model.Hero;

import java.util.List;

public interface HeroService {
    int addHero(Hero hero);
    List<Hero> selectHeroesFuzzy(String heroName);
    List<Hero> selectHeroes();
    int updateHero(Hero hero);
    Hero selectHero(Hero hero);
    Hero selectHeroByName(Hero hero);
    int deleteHero(Hero hero);
}
