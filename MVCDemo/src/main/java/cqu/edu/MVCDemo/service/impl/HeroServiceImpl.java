package cqu.edu.MVCDemo.service.impl;

import cqu.edu.MVCDemo.dao.HeroDao;
import cqu.edu.MVCDemo.model.Hero;
import cqu.edu.MVCDemo.service.HeroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class HeroServiceImpl implements HeroService {
    @Resource
    HeroDao heroDao;

    @Override
    public int addHero(Hero hero) {
        return heroDao.insertHero(hero);
    }

    @Override
    public List<Hero> selectHeroes() {
        return heroDao.selectHeroes();
    }

    @Override
    public Hero selectHero(Hero hero) {
        return heroDao.selectHero(hero);
    }

    @Override
    public Hero selectHeroByName(Hero hero) {
        return heroDao.selectHeroByName(hero);
    }

    @Override
    public int updateHero(Hero hero) {
        return heroDao.updateHero(hero);
    }

    @Override
    public int deleteHero(Hero hero) {
        return heroDao.deleteHero(hero);
    }

    @Override
    public List<Hero> selectHeroesFuzzy(String heroName) {
        return heroDao.selectHeroesFuzzy(heroName);
    }
}
