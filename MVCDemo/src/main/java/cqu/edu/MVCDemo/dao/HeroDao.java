package cqu.edu.MVCDemo.dao;

import cqu.edu.MVCDemo.model.Hero;

import java.util.List;

public interface HeroDao {
    int insertHero(Hero hero);

    List<Hero> selectHeroes();

    int updateHero(Hero hero);

    int deleteHero(Hero hero);

    Hero selectHero(Hero hero);

    Hero selectHeroByName(Hero hero);

    List<Hero> selectHeroesFuzzy(String heroName);
}
