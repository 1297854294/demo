package cqu.edu.MVCDemo.exception;

import cqu.edu.MVCDemo.exception.DemoException;

public class HeroNameException extends DemoException {
    public HeroNameException() {
    }

    public HeroNameException(String message) {
        super(message);
    }
}
