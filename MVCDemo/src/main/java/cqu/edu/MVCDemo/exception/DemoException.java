package cqu.edu.MVCDemo.exception;

public class DemoException extends Exception{
    public DemoException() {
        super();
    }

    public DemoException(String message) {
        super(message);
    }
}
