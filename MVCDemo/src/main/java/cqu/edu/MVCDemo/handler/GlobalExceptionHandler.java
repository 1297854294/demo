package cqu.edu.MVCDemo.handler;

import cqu.edu.MVCDemo.exception.HeroNameException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;


@ControllerAdvice
@ResponseBody
public class GlobalExceptionHandler {

    @ExceptionHandler(value = HeroNameException.class)
    public  String heroNameExceptionHandler(Exception e){
        System.out.println("exception");
        return e.getMessage();
    }

    @ExceptionHandler(value = Exception.class)
    public  String exceptionHandler(Exception e){
        return e.getMessage();
    }
}
